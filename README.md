# Bentoak

for runnig project:

1 - npm install

2 - npm start

* you must have NODE JS installted!

Techs: 

This project using React js version 17.0.3

for much faster work I used antd library https://ant.design/

for styles I just use css


for login using this info from: https://reqres.in/

Email : eve.holt@reqres.in

Password: cityslicka


Flow:

in the message menu for delete swap to right!

for undo delete you can swap to the left or after clicking on the Delete button choice no!

swap for delete only works on mobiles!

delete functionality just work offline because lack of good fake api in https://reqres.in/ !

you can filter users with a bar at the top, bar filter users with their names.

you can logout from system by clicking on profile image on the inbox page.



About Project:

1 - I don't use any sort of state management techs because the scale of the project was small, but for future-proofing, you can easily adding context or other techs like redux and etc.

2- the fake API that is used in the project doesn't have any token base auth for exp or any secure methods so I have to just store the token in the local storage.

3 - the design principles that were sent to me, don't have any standards so I did not follow that and changing the design and language to en.

4 - for lack of source in the test document I had to use another project for icons for pwa manifest file.

4 - Main code Located in Development Branch
