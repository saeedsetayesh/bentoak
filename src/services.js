import axios from "axios";

export const baseUrl = process.env.REACT_APP_BASE_URL;

export const login = (body) => {
  return axios.post(baseUrl + "login", body);
};

export const getUsers = () => {
  return axios.get(baseUrl + "users/?per_page=12");
};

export const deleteUser = (id) => {
  return axios.get(baseUrl + `users/${id}`);
};
