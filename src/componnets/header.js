import React from "react";
import bentoak from "./../assets/bentoak.png";
import profile from "./../assets/avatar.png";
import filter from "./../assets/filter.png";
import { useHistory } from "react-router";

const Header = () => {
  const history = useHistory();
  return (
    <div className="header">
      <img src={filter} alt="filter"></img>
      <img src={bentoak} alt="logo"></img>
      <img
        onClick={() => {
          localStorage.clear();
          history.push("/login");
        }}
        src={profile}
        alt="avatar"
      ></img>
    </div>
  );
};

export default Header;
