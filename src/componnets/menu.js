import React from "react";
import edit from "./../assets/edit.png";
import inbox from "./../assets/inbox.png";

const Menu = () => {
  return (
    <div className="menu-sticky">
      <img src={edit} alt="edit"></img>
      <img src={inbox} alt="inbox"></img>
    </div>
  );
};

export default Menu;
