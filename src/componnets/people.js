import React from "react";
import { RightOutlined } from "@ant-design/icons";

export const People = ({ data }) => {
  return (
    <div className="people-card" id={data.id}>
      <div className="people-image">
        <img src={data.avatar} alt={data.first_name}></img>
        <div className="people-detailes">
          <p className="font-bold">
            {data.first_name} {data.last_name}
          </p>
          <p>{data.email}</p>
        </div>
      </div>

      <RightOutlined />
    </div>
  );
};
