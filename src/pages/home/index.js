import React, { useEffect, useState } from "react";
import { People } from "./../../componnets/people";
import { getUsers } from "./../../services";
import Header from "./../../componnets/header";
import Menu from "./../../componnets/menu";
import { Modal } from "antd";
import { success } from "./../../componnets/helper";
const Home = () => {
  const [userData, setUserData] = useState([]);
  const [userItem, setUserItem] = useState({});
  const [allUsers, setAllUsers] = useState([]);
  const [touchStart, setTouchStart] = useState(0);
  const [touchEnd, setTouchEnd] = useState(0);
  const [visible, setVisible] = useState(false);
  useEffect(() => {
    getUsers().then((res) => {
      setUserData(res.data.data);
      setAllUsers(res.data.data);
    });
  }, []);

  const filterItems = (e, name) => {
    setAllUsers(userData);
    if (name === "All") {
      setUserData(allUsers);
    } else {
      setUserData(userData.filter((item) => item.first_name === name));
    }
  };

  const handleTouchStart = (e) => {
    setTouchStart(e.targetTouches[0].clientX);
  };

  const handleTouchMove = (e) => {
    setTouchEnd(e.targetTouches[0].clientX);
  };

  const hideSwap = (id) => {
    document.getElementById(`delete-${id}`).style.left = "0px";
    document.getElementById(`span-${id}`).style.display = "none";
  };

  const handleTouchEnd = (id) => {
    if (touchStart - touchEnd > 80) {
      hideSwap(id);
    }

    if (touchStart - touchEnd < -80) {
      document.getElementById(`delete-${id}`).style.left = "100px";
      document.getElementById(`span-${id}`).style.display = "inline";
    }
  };

  const removeItem = () => {
    setUserData(userData.filter((item) => item.id !== userItem.id));
    setVisible(false);
    hideSwap(userItem.id);
    success(`${userItem.first_name} Deleted`);
  };

  const closeConfirm = () => {
    setVisible(false);
    hideSwap(userItem.id);
  };

  return (
    <div className="container">
      <Menu />
      <Header />

      <div className="filter">
        <p
          onClick={(e) => {
            filterItems(e, "All");
          }}
          className="filter-item"
        >
          All
        </p>
        {userData.map((item) => (
          <p
            onClick={(e) => {
              filterItems(e, item.first_name);
            }}
            className="filter-item"
          >
            {item.first_name}
          </p>
        ))}
      </div>
      <div className="users-list">
        {userData.map((item) => (
          <div
            onTouchEnd={() => {
              handleTouchEnd(item.id);
            }}
            onTouchMove={handleTouchMove}
            onTouchStart={handleTouchStart}
            className="delete-swap"
            id={`delete-${item.id}`}
          >
            <span
              onClick={() => {
                setUserItem(item);
                setVisible(!visible);
              }}
              id={`span-${item.id}`}
              className="delete"
            >
              Delete
            </span>
            <People data={item} />
          </div>
        ))}
      </div>
      <Modal
        title="Delete"
        visible={visible}
        onOk={removeItem}
        onCancel={closeConfirm}
        okText="Yes"
        cancelText="No"
      >
        <p>Are You Sure Want to Delete {userItem.first_name} ?</p>
      </Modal>
    </div>
  );
};

export default Home;
