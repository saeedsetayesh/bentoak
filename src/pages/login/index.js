import React, { useState, useEffect } from "react";
import { Form, Input, Button, Typography } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { login } from "./../../services";
import { error, success } from "./../../componnets/helper";
import { useHistory } from "react-router";
import bentoak from "./../../assets/logo-login.png";

const { Title, Text } = Typography;

const Login = () => {
  const [loader, setLoader] = useState(false);
  const history = useHistory();

  const onFinish = async (values) => {
    setLoader(true);
    await login(values).then(
      (res) => {
        localStorage.setItem("token", res.data.token);
        success("Welcome!");
        setLoader(false);
        history.push("/");
      },
      (err) => {
        setLoader(false);
        error(err.response.data.error);
      }
    );
  };

  useEffect(() => {
    if (localStorage.getItem("token")) {
      history.push("/");
    }
  });

  return (
    <div className="container">
      <div className="login-main">
        <div className="logo-login">
          <img src={bentoak} alt="logo"></img>
        </div>
        <Title level={4}>Already Signed Up?</Title>
        <Text>Login with your email and password</Text>

        <Form
          name="normal_login"
          className="login-form mt-5"
          initialValues={{ remember: true }}
          onFinish={onFinish}
        >
          <Form.Item
            name="email"
            rules={[{ required: true, message: "Please input your email!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button w-100"
              loading={loader}
            >
              Log in
            </Button>
            <p className="mt-5">
              Don't Have an Account?{" "}
              <a href="https://google.com">Register Now!</a>
            </p>

            <p className="mt-5">
              Forget Your Password? <a href="https://google.com">Click Here!</a>
            </p>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default Login;
